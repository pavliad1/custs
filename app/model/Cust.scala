package model

import scala.collection.mutable.ListBuffer
import scala.collection.mutable.ListMap
import play.api.libs.json.Json
import play.api.libs.json._

case class Cust(id: Int, name: String, email: String)

object Cust {
  def apply(name: String, email: String) : Cust = apply(0, name, email)
  def unapply(arg: Cust): Option[(String, String)] = Some(arg.name, arg.email)

  implicit object CustomerFormat extends Format[Cust] {
    def writes(cust: Cust) : JsValue = {
      JsObject(Seq(
        "id" -> JsNumber(cust.id),
        "name" -> JsString(cust.name),
        "email" -> JsString(cust.email)
      ))
    }

    def reads(json: JsValue) : JsResult[Cust] = {
      // nepouziju
      JsSuccess(Cust(0, "", ""))
    }
  }
}

object Custs {


  val custs = ListBuffer( Cust(1, "Adam", "adam@fit.cz"), Cust(2, "Lukas", "lukas@fit.cz"), Cust(3, "Pepa", "pepa@fit.cz"))

  def toJsonString(): String = {
    var res = ""
    custs.foreach( cust => res += s"${Json.toJson(cust)}\n" )
    res
  }

  def getNextID() = if (custs.isEmpty) 1 else custs.sortWith( (c1, c2) => c1.id > c2.id ).head.id + 1

  def add(name: String, email: String) : Boolean = {
    if (!custs.filter( c => c.name == name).isEmpty) return false
    custs.addOne(Cust(getNextID(), name, email))
    true
  }

  def add(cust: Cust) : Boolean = {
    if (!custs.filter( c => c.name == cust.name).isEmpty) return false
    custs.addOne(cust.copy(getNextID()))
    true
  }

  def delete(id: Int): Boolean = {
    val origSize = custs.length
    custs.filterInPlace( cust => cust.id != id )
    if (origSize == custs.length) false else true
  }

  def update(id: Int, nname: String, nemail: String) : Boolean = {
    val cust = custs.filter( cust => cust.id == id )
    if (cust.isEmpty)
      false
    else {
      val ncust = cust.head.copy(name = nname, email = nemail)
      custs.mapInPlace( cust => if (cust.id == id) ncust else cust )
      true
    }
  }
}