package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import model.Cust

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  val custs = model.Custs
  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def listcustomers() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.listcustomers(custs.custs)) }

  def add_customer()= Action { implicit request: Request[AnyContent] =>
    val name = request.body.asFormUrlEncoded.head("username").head
    val email =  request.body.asFormUrlEncoded.head("email").head
    if (email == "" || name == "")
      BadRequest("Error adding customer.")
    if (!custs.add(name,email))
      BadRequest(s"Customer ${name} already exists.")
    else
    Redirect(routes.HomeController.listcustomers())
  }

  def delete_customer()= Action { implicit request: Request[AnyContent] =>
    var id: Int = -1
    try {
      id = request.body.asFormUrlEncoded.head("id").head.toInt
    }
    catch {
      case _: Throwable => BadRequest("Wrong id input.")
    }
    if (id <= 0)
      BadRequest("Wrong id input.")
    else if (!custs.delete(id))
      BadRequest(s"Customer with id ${id} not found.")
    else
      Redirect(routes.HomeController.listcustomers())
  }

  def update_customer()= Action { implicit request: Request[AnyContent] =>
    val name = request.body.asFormUrlEncoded.head("username").head
    val email =  request.body.asFormUrlEncoded.head("email").head
    var id: Int = -1
    try {
      id = request.body.asFormUrlEncoded.head("id").head.toInt
    }
    catch {
      case _: Throwable => BadRequest("Wrong id input.")
    }
    if (id <= 0)
      BadRequest("Wrong id input.")
    else if (email == "" || name == "")
      BadRequest("Empty name/email input.")
    else if (!custs.update(id, name, email))
      BadRequest(s"Customer with id ${id} not found.")
    else
      Redirect(routes.HomeController.listcustomers())
  }
}
