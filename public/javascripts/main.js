/**
 * Retrieves input data from a form and returns it as a JSON object.
 * @param  {HTMLFormControlsCollection} elements  the form elements
 * @return {Object}                               form data as an object literal
 */
const formToJSON = elements => [].reduce.call(elements, (data, element) => {
  data[element.name] = element.value;
  return data;
}, {});
const handleFormSubmit = event => {
//alert("submit!")
  // Stop the form from submitting since we’re handling that with AJAX.
  event.preventDefault();
  // Call our function to get the form data.
  const data = formToJSON(form.elements);

  alert(JSON.stringify(data))

  // construct an HTTP request
  var xhr = new XMLHttpRequest();
  xhr.open(form.method, form.action, true);
  xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

  // send the collected data as JSON
  xhr.send(JSON.stringify(data));
  // ...this is where we’d actually do something with the form data...

  //xhr.onloadend = function () {
  //    alert("okkkk")
  // };
};